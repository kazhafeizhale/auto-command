﻿/*
*----------------说明区----------------------
*功能：autocomplete命令输入，按键：shift+CapsLock 或者 双击ctrl
*环境：win10
*作者：kazhafeizhale
*时间：2019年4月16日
*使用:
*   - 双击 ctrl启动 或者 shift+capslock
*	- 然后输入命令，例如“百度”
*   - 这个只是个模板，在CommandText列表中放入自己的命令,并添加标签。
*   - 支持拼音首字母搜索,多个搜索条件用空格隔开
*/
#include <py>
#SingleInstance force
SetTitleMatchMode, 1


;管理员运行
full_command_line := DllCall("GetCommandLine", "str")
if not (A_IsAdmin or RegExMatch(full_command_line, " /restart(?!\S)"))
{
    try
    {
        if A_IsCompiled
            Run *RunAs "%A_ScriptFullPath%" /restart
        else
            Run *RunAs "%A_AhkPath%" /restart "%A_ScriptFullPath%"
    }
    ExitApp
}
msgbox, 双击ctrl或者 shift+CapsLock 呼出面板 `n 输入 命令 查看所有命令 `n 支持拼音首字母搜索 例如输入 ml
;-----------------------------------------------------

Commands := []
gosub, CommandListing
Loop, Parse, CommandText, `n
{
	Loop, Parse, A_LoopField, `;
	{
		if (A_Index = 1)
		{
			if (A_LoopField != LastCommand)
			{
				CurrentCommand := A_LoopField
				Commands.Push(CurrentCommand)
				LastCommand := CurrentCommand
			}
		}
	}
}
;-----------------gui界面设置----------------------

Gui, -Caption +AlwaysOnTop +ToolWindow
WinSet, TransColor, Off,
Gui, Font, s10 cLime, Consolas
Gui, Margin, 0, 0
Gui, Color,,0x000000
Gui, Add, Edit, w170 h20 vCommandSearch gCommandTypingEvent
Gui, Add, ListBox, h0 w170 vCommandChoice -HScroll

Gui, Show, Hide AutoSize, Get Parameters

return

;智能命令，双击 ctrl 执行
Ctrl::
if winc_presses > 0 ; SetTimer 已经启动, 所以我们记录键击.
{
    winc_presses += 1
    return
}
; 否则, 这是新开始系列中的首次按下. 把次数设为 1 并启动
; 计时器:
winc_presses = 1
SetTimer, KeyWinC, -400 ; 在 400 毫秒内等待更多的键击.
return

+CapsLock::
	GuiControl,, CommandSearch ; clear entry
	GuiControl,, CommandChoice, | ; clear list
	GuiControl, Hide, CommandChoice
    with:=A_ScreenWidth/2
    height:=A_ScreenHeight/2
	Gui, Show, x%with% y%height% AutoSize
return

#IfWinActive, Get Parameters ahk_exe AutoHotkey.exe 

Escape::
	Gui, Cancel
return

Up::
	if (HighlightedCommand > 1)
	{
		HighlightedCommand--
		GuiControl, Choose, CommandChoice, %HighlightedCommand%
		Gui, Show, AutoSize		
	}
return

Down::
	if (HighlightedCommand < ListCount)
	{
		HighlightedCommand++
		GuiControl, Choose, CommandChoice, %HighlightedCommand%
		Gui, Show, AutoSize
	}
return

~Enter::
    SendInput,{shift}
	GuiControlGet, SelectedCommand,, CommandChoice
	if (!SelectedCommand || !CommandEntry || !ListCount)
	{
		Gui, Cancel
		return
	}
	Gui, Cancel
	CurrentCaretX := A_CaretX
    Gosub,% SelectedCommand
return

;双击执行跳转 
KeyWinC:
if winc_presses = 2 ; 此键按下了两次.
{
	GuiControl,, CommandSearch ; clear entry
	GuiControl,, CommandChoice, | ; clear list
	GuiControl, Hide, CommandChoice
    with:=A_ScreenWidth/2
    height:=A_ScreenHeight/2
	Gui, Show, x%with% y%height% AutoSize
}
; 不论触发了上面的哪个动作, 都对 count 进行重置
; 为下一个系列的按下做准备:
winc_presses = 0
return

CommandTypingEvent:
	GuiControlGet, CommandEntry,, CommandSearch
	PipedCommandList := "|"
	ListCount := 0
	if CommandEntry
	{
		Loop, % Commands.MaxIndex()
		{
			if(keyValueFind(Commands[A_Index], CommandEntry))
			{
				PipedCommandList .= Commands[A_Index] "|" 
				ListCount++
			}		
		}
	}
	GuiControl, Move, CommandChoice, % "h" 4 + 15 * (ListCount > 4 ? 5 : ListCount)
	GuiControl, % ListCount ? "Show" : "Hide", CommandChoice
	StringTrimRight, PipedCommandList, PipedCommandList, 1 ; remove last pipe
	GuiControl,, CommandChoice, %PipedCommandList%
	HighlightedCommand := 1
	GuiControl, Choose, CommandChoice, 1
	Gui, Show, AutoSize
return

TaskDialog(Instruction, Content := "", Title := "", Buttons := 1, IconID := 0, IconRes := "", Owner := 0x10010) {
    Local hModule, LoadLib, Ret

    If (IconRes != "") {
        hModule := DllCall("GetModuleHandle", "Str", IconRes, "Ptr")
        LoadLib := !hModule
            && hModule := DllCall("LoadLibraryEx", "Str", IconRes, "UInt", 0, "UInt", 0x2, "Ptr")
    } Else {
        hModule := 0
        LoadLib := False
    }
    DllCall("TaskDialog"
        , "Ptr" , Owner        ; hWndParent
        , "Ptr" , hModule      ; hInstance
        , "Ptr" , &Title       ; pszWindowTitle
        , "Ptr" , &Instruction ; pszMainInstruction
        , "Ptr" , &Content     ; pszContent
        , "Int" , Buttons      ; dwCommonButtons
        , "Ptr" , IconID       ; pszIcon
        , "Int*", Ret := 0)    ; *pnButton

    If (LoadLib) {
        DllCall("FreeLibrary", "Ptr", hModule)
    }
    Return {1: "OK", 2: "Cancel", 4: "Retry", 6: "Yes", 7: "No", 8: "Close"}[Ret]
}

命令:
Instruction := CommandText
Content := ""
TaskDialog(Instruction, Content, "", 0x1, 0)
return

斗鱼:
Run,https://www.douyu.com/
return

龙珠:
Run,http://www.longzhu.com/
return

战旗:
Run,https://www.zhanqi.tv/
return

锁屏:
SendInput,#R
return

ahk:
Run,https://www.autoahk.com/
return

百度:
Run,https://www.baidu.com/
return

虎牙:
Run,https://www.huya.com/
Return

论坛:
Run,https://autohotkey.com/boards/
return

github:
run,https://github.com/
Return

画图:
Run,*RunAs mspaint.exe,%windir%\system32\
return

CommandListing:
CommandText = 
(Join`n %
;======网页===
虎牙;浏览器打开虎牙
斗鱼;
龙珠;
战旗;
ahk;
画图;
百度;
命令;显示所以命令
)

return
keyValueFind(haystack,needle)
{
    ;拼音首字母转换
    haystack .= py.initials(haystack)
	findSign:=1
	needleArray := StrSplit(needle, " ")
	Loop,% needleArray.MaxIndex()
	{
		if(!InStr(haystack, needleArray[A_Index], false))
		{
			findSign:=0
			break
		}	
	}
	return findSign
}